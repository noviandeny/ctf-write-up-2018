# HEEEEEEERE'S Johnny! - Points: 100 - (Solves: 2344) 

Description :
> Okay, so we found some important looking files on a linux computer. Maybe they can be used to get a password to the process. Connect with nc 2018shell1.picoctf.com 42165. Files can be found here: passwd shadow.

Hints:
> If at first you don't succeed, try, try again. And again. And again.  
> If you're not careful these kind of problems can really "rockyou".

Karena diberikan file passwd dan shadow, asumsi saya disini kita harus mencari tahu password dari user di file tersebut.
```
$ cat shadow  
>root:$6$q7xpw/2.$la4KiUz87ohdszbOVoIopy2VTwm/5jEXvWSdWynh0CnP5T.MnJfVNCzp3IfJMHUNuBhr1ewcYd8PyeKHqHQoe.:17770:0:99999:7:::

$ cat passwd
> root:x:0:0:root:/root:/bin/bash
```
Untuk soal semacam ini, ada tool namanya `john` (john the ripper)
> John the Ripper is a fast password cracker, currently available for many flavors of Unix, Windows, DOS, and OpenVMS. Its primary purpose is to detect weak Unix passwords. Besides several crypt(3) password hash types most commonly found on various Unix systems, supported out of the box are Windows LM hashes, plus lots of other hashes and ciphers in the community-enhanced version. 

Langkah pertama adalah, kita `unshadow` dulu file passwd dan shadow.  
> `Usage: unshadow PASSWORD-FILE SHADOW-FILE`
> `unshadow passwd shadow > traditional_UNIX_password`

Lalu gunakan `john`:
> `john traditional_UNIX_password`
```
Warning: detected hash type "sha512crypt", but the string is also recognized as "sha512crypt-opencl"
Use the "--format=sha512crypt-opencl" option to force loading these as that type instead
Warning: detected hash type "sha512crypt", but the string is also recognized as "crypt"
Use the "--format=crypt" option to force loading these as that type instead
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 64/64 OpenSSL])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
**kissme           (root)**
1g 0:00:00:02 DONE 2/3 (2018-10-13 10:38) 0.3610g/s 874.7p/s 874.7c/s 874.7C/s keller..mermaid
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

Ketemu passwordnya **kissme**, langsung nc ke service yang diberikan

```
Username: root
Password: kissme
**picoCTF{J0hn_1$_R1pp3d_5f9a67aa}**
```

Flag : picoCTF{J0hn_1$_R1pp3d_5f9a67aa}


_NOTE_ : hint "rockyou" mungkin mengacu pada wordlist "rockyou"